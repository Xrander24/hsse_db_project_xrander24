update anime_db.character
set character_desc = 'A childhood friend of the central characters, the Elric brothers, Winry is often seen in their company throughout the series. She is evidently Edward''s love interest. Specializing in mechanical repair, specifically with automail, Winry services Edward''s arm and leg whenever it is in need of repair or replacement.'
where character_nm = 'Winry Rockbell';

update anime_db.origin
set chapters_cnt = 97
where origin_nm = 'Spy x Family';

update anime_db.origin
set chapters_cnt = 297
where origin_nm = 'One Punch Man';

update anime_db.origin
set chapters_cnt = 119
where origin_nm = 'That Time I Got Reincarnated as a Slime';

update anime_db.anime
set episodes_cnt = episodes_cnt + 26
where anime_nm = 'Bleach';

--actualize age for all actors
update anime_db.actor
set age = date_part('year', (cast('0001-01-01' as date) + (now() - birth_dt))) - 1;

delete
from anime_db.character
where character_nm = 'Shirley Fenette';