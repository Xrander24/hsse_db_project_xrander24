create schema if not exists anime_db;

create table anime_db.studio
(
    studio_id     serial primary key,
    studio_nm     varchar(70) not null,
    employees_cnt int,
    site_url      text
);

create table anime_db.anime
(
    anime_id       serial primary key,
    anime_nm       varchar(100) not null,
    episodes_cnt   int,
    genre          text,
    anime_desc     text,
    publication_dt date
);

create table anime_db.origin
(
    origin_id      serial primary key,
    origin_nm      varchar(100) not null,
    author_nm      varchar(70),
    publication_dt date,
    chapters_cnt   int,
    anime_id       serial,
    foreign key (anime_id) references anime_db.anime
);

create table anime_db.actor
(
    actor_id   serial primary key,
    actor_nm   varchar(70) not null,
    age        int,
    birth_dt   date,
    social_url text
);


create table anime_db.anime_x_studio
(
    anime_id  serial,
    studio_id serial,
    foreign key (anime_id) references anime_db.anime,
    foreign key (studio_id) references anime_db.studio
);

create table anime_db.character
(
    character_id   serial,
    character_nm   varchar(100),
    anime_id       serial,
    actor_id       serial,
    character_desc text
);

alter table anime_db.character
    add primary key (character_id);
alter table anime_db.character
    alter column character_nm set not null;
alter table anime_db.character
    add constraint FK_character_anime
        foreign key (anime_id) references anime_db.anime;
alter table anime_db.character
    add constraint FK_character_actor
        foreign key (actor_id) references anime_db.actor;