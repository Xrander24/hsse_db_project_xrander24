--триггер для поддержки актуального возраста актера
create or replace function actualize_actor_age()
    returns trigger as
$$
begin
    new.age = date_part('year', (cast('0001-01-01' as date) + (now() - new.birth_dt))) - 1;
    return new;
end;
$$ language plpgsql;

create or replace trigger actor_age_tr
    before update or insert
    on anime_db.actor
    for each row
execute procedure actualize_actor_age();

--топ N самых больших студий
create or replace function top_n_biggest_studios(n int)
    returns table
            (
                studio_name   text,
                employees_cnt int
            )
as
$$
select studio_nm, employees_cnt
from anime_db.studio
where employees_cnt is not null
order by employees_cnt desc
limit n;
$$ language sql;

--Получить время года по дате
create or replace function get_season(date) returns text as
$$
declare
    month int = date_part('month', $1);
begin
    if (month between 3 and 5) then
        return 'spring';
    elseif (month between 6 and 8) then
        return 'summer';
    elseif (month between 9 and 11) then
        return 'fall';
    else
        return 'winter';
    end if;
end;
$$ language plpgsql;

--Получить все аниме какого-то сезона, например все аниме весны 2023
create or replace function all_anime_of_season(year int, season text)
    returns table
            (
                id       int,
                name     text,
                episodes int,
                genre    text
            )
as
$$
begin
    return query (select anime_id, cast(anime_nm as text), episodes_cnt, anime_db.anime.genre
                  from anime_db.anime
                  where date_part('year', publication_dt) = year
                    and get_season(publication_dt) = season);
end;
$$ language plpgsql;