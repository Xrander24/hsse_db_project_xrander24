--посчитать сколько разных аниме по каждому жанру
select genre, count(*) as cnt
from anime_db.anime
group by genre;

--сколько аниме выпустила студия
select studio_nm, count(*) as cnt
from anime_db.studio
         join anime_db.anime_x_studio on studio.studio_id = anime_x_studio.studio_id
group by anime_db.studio.studio_id
order by cnt;

--получить всех персонажей из аниме Код Гиасс
select character_nm
from anime_db.character
         join anime_db.anime on character.anime_id = anime.anime_id
where anime_nm = 'Code Geass: Lelouch of the Rebellion';

--все аниме у которых кол-во серий не меньше кол-ва глав у манги
select origin_nm
from anime_db.origin
         join anime_db.anime on origin.anime_id = anime.anime_id
where chapters_cnt <= episodes_cnt;


--сколько лет было каждому актеру на момент исполнения конкретной роли
select actor_nm,
       character_nm,
       anime_nm,
       date_part('year', (cast('0001-01-01' as date) + (publication_dt - birth_dt))) - 1 as age_of_actor
from anime_db.actor
         join (select actor_id, publication_dt, anime_nm, character_nm
               from anime_db.character
                        join anime_db.anime on character.anime_id = anime.anime_id) as anime_of_character
              on anime_of_character.actor_id = actor.actor_id
order by anime_of_character.actor_id;

--максимальное число серий в сериале для жанра
select distinct genre, max(episodes_cnt) over (partition by genre) as max_cnt_of_episodes
from anime_db.anime;

--скользящее среднее по числу эпизодов зачем-то
select anime_nm, round(avg(episodes_cnt) over (order by episodes_cnt rows between 1 preceding and 1 following), 3)
from anime_db.anime;

--cамое старое аниме в жанре
with min_date as (select min(publication_dt) over (partition by genre) as min_date
                  from anime_db.anime)
select distinct genre, anime_nm, publication_dt
from anime_db.anime,
     min_date
where publication_dt = min_date.min_date;

--разница возраста актера и среднего возраста в процентах
select actor_nm,
       age,
       round(((age - avg(age) over (order by age rows between unbounded preceding and unbounded following)) / age *
              100), 3) as diff_in_percents
from anime_db.actor;

--разбиение манг по количеству глав на 3 группы
select origin_nm, chapters_cnt, ntile(3) over (order by chapters_cnt)
from anime_db.origin;