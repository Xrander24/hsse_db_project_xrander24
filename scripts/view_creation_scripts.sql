--сводная таблица для персонажей с названием аниме и именем актера
create or replace view characters_full_info as
(
select character_nm,
       character_desc,
       anime_nm,
       actor_nm
from anime_db.character
         join anime_db.anime on character.anime_id = anime.anime_id
         join anime_db.actor on character.actor_id = actor.actor_id
    );

--аниме со списком всех студий когда-либо работавших над ним
create or replace view anime_with_studio as
(
with anime_studio as (select *
                      from anime_db.anime
                               join anime_db.anime_x_studio on anime.anime_id = anime_x_studio.anime_id
                               join anime_db.studio on anime_x_studio.studio_id = studio.studio_id)
select distinct anime_nm,
                anime_desc,
                array_agg(anime_studio.studio_nm) as studios,
                episodes_cnt
from anime_studio
group by anime_nm, anime_desc, episodes_cnt
    );

--все экшн сериалы
create or replace view action_anime as
(
select anime_nm, anime_desc, episodes_cnt, genre
from anime_db.anime
where genre = 'Action'
    )
with cascaded check option;

--сводная таблица для аниме с указанием оригинального произведения
create or replace view anime_with_origin as
(
select anime_nm,
       anime_desc,
       episodes_cnt,
       chapters_cnt,
       author_nm
from anime_db.anime
         join anime_db.origin on anime.anime_id = origin.anime_id);
